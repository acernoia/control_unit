#ifndef CONTROL_UNIT_HPP
#define CONTROL_UNIT_HPP

SC_MODULE(control_unit)
{
   //INGRESSI
   sc_in<sc_uint<3> >   opcode;
   sc_in<sc_uint<16> >  eq;
   //USCITE
   sc_out<sc_uint<1> >  sel_ALU1, sel_ALU2;
   sc_out<sc_uint<2> >  func_ALU;
   sc_out<sc_uint<1> >  sel_RF, load_RF;  
   sc_out<sc_uint<2> >  sel_TGT;
   sc_out<sc_uint<1> >  write_mem;
   sc_out<sc_uint<2> >  sel_PC;
   SC_CTOR(control_unit)
   {
     SC_THREAD(controlla);
     sensitive << opcode << eq;
   }
   private:
   void controlla();
};


#endif
