#include <systemc.h>
#include "control_unit.hpp"

using namespace std;

void control_unit::controlla()
{
   //Inizializza la control unit con la funzione ADD
   func_ALU->write(0);  //funzione ADD della ALU
   load_RF->write(1);   //il risultato va scritto nel register file
   sel_RF->write(1);    //il register file prende SRC2 dal campo istruzione rC
   sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
   sel_ALU2->write(1);  //la ALU prende SRC2 dal register file
   sel_TGT->write(1);   //il risultato della ALU va scritto nel register file
   write_mem->write(0); //non scrivere il risultato nella memoria dati
   sel_PC->write(2);    //incrementa il program counter
   while(true)
   {
      wait();
      switch(opcode->read())
      {
         //ADD
         case 0: func_ALU->write(0);  //funzione ADD della ALU
                 load_RF->write(1);   //il risultato va scritto nel register file
                 sel_RF->write(1);    //il register file prende SRC2 dal campo istruzione rC
                 sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
                 sel_ALU2->write(1);  //la ALU prende SRC2 dal register file
                 write_mem->write(0); //non scrivere il risultato nella memoria dati
                 sel_PC->write(2);    //incrementa il program counter
                 sel_TGT->write(1);   //il risultato della ALU va scritto nel register file
                 break;
         //ADDI
         case 1: func_ALU->write(0);  //funzione ADD della ALU
                 load_RF->write(1);   //il risultato va scritto nel register file
                 sel_RF->write(0);    //indifferente
                 sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
                 sel_ALU2->write(0);  //la ALU prende SRC2 dal campo istruzione immediate
                 write_mem->write(0); //non scrivere il risultato nella memoria dati
                 sel_PC->write(2);    //incrementa il program counter
                 sel_TGT->write(1);   //il risultato della ALU va scritto nel register file
                 break;

         //NAND
         case 2: func_ALU->write(1);  //funzione NAND della ALU
                 load_RF->write(1);   //il risultato va scritto nel register file
                 sel_RF->write(1);    //il register file prende SRC2 dal campo istruzione rC
                 sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
                 sel_ALU2->write(1);  //la ALU prende SRC2 dal register file
                 sel_TGT->write(1);   //il risultato della ALU va scritto nel register file
                 write_mem->write(0); //non scrivere il risultato nella memoria dati
                 sel_PC->write(2);    //incrementa il program counter
                 break;

         //LUI : load upper intermediate
         case 3: func_ALU->write(2);  //funzione PASS1 della ALU
                 load_RF->write(1);   //il risultato va scritto nel register file
                 sel_RF->write(0);    //indifferente
                 sel_ALU1->write(0);  //la ALU prende SRC1 dal campo istruzione immediate
                 sel_ALU2->write(0);  //indifferente
                 sel_TGT->write(1);   //il risultato della ALU va scritto nel register file
                 write_mem->write(0); //non scrivere il risultato nella memoria dati
                 sel_PC->write(2);    //incrementa il program counter
                 break;

         //LW : load data word
         case 4: func_ALU->write(0);  //funzione ADD della ALU
                 load_RF->write(1);   //il risultato va scritto nel register file
                 sel_RF->write(0);    //indifferente
                 sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
                 sel_ALU2->write(0);  //la ALU prende SRC2 dal sign extend
                 sel_TGT->write(2);   //scrivi nel register file il dato preso dalla memoria
                 write_mem->write(0); //non scrivere il risultato nella memoria dati
                 sel_PC->write(2);    //incrementa il program counter
                 break;

         //SW : store data word
         case 5: func_ALU->write(0);  //funzione ADD della ALU
                 load_RF->write(0);   //il risultato NON va scritto nel register file
                 sel_RF->write(0);    //il register file prende SRC2 dal campo istruzione rA
                 sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
                 sel_ALU2->write(0);  //la ALU prende SRC2 dal sign extend
                 sel_TGT->write(0);   //indifferente
                 write_mem->write(1); //scrivi il risultato nella memoria dati
                 sel_PC->write(2);    //incrementa il program counter
                 break;

         //BEQ : branch if equal NON COMPLETATA
         case 6: func_ALU->write(3);  //funzione EQ? della ALU
                 load_RF->write(0);   //il risultato NON va scritto nel register file
                 sel_RF->write(0);    //il register file prende SRC2 dal campo istruzione rA
                 sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
                 sel_ALU2->write(1);  //la ALU prende SRC2 dal register file
                 sel_TGT->write(0);   //indifferente
                 write_mem->write(0); //non scrivere il risultato nella memoria dati
                 if (eq->read()==1)
                      sel_PC->write(1);    //branch
                 else
                      sel_PC->write(2);    //incrementa il program counter
                 break;
   
         //JALR : jump and link through register
         case 7: func_ALU->write(2);  //funzione PASS1 della ALU
                 load_RF->write(1);   //il risultato va scritto nel register file
                 sel_RF->write(0);    //SRC2 indifferente
                 sel_ALU1->write(1);  //la ALU prende SRC1 dal register file
                 sel_ALU2->write(1);  //indifferente
                 sel_TGT->write(0);   //target=pc+1
                 write_mem->write(0); //non scrivere il risultato nella memoria dati
                 sel_PC->write(0);    //prendi il PC dal register file
                 break;
      }
   }
}
