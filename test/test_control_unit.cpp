#include <systemc.h>
#include <string>
#include "control_unit.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<3> >   opcode_s;
    sc_signal<sc_uint<16> >  eq_s;
    sc_signal<sc_uint<1> >   sel_ALU1_s, sel_ALU2_s;
    sc_signal<sc_uint<2> >   func_ALU_s;
    sc_signal<sc_uint<1> >   sel_RF_s, load_RF_s;  
    sc_signal<sc_uint<2> >   sel_TGT_s, sel_PC_s;
    sc_signal<sc_uint<1> >   write_mem_s;

    control_unit ctrl;

    SC_CTOR(TestBench) : ctrl("ctrl")
    {
        SC_THREAD(stimulus_thread);
        ctrl.opcode(this->opcode_s);
        ctrl.eq(this->eq_s);
        ctrl.sel_ALU1(this->sel_ALU1_s);
        ctrl.sel_ALU2(this->sel_ALU2_s);
        ctrl.func_ALU(this->func_ALU_s);
        ctrl.sel_RF(this->sel_RF_s);
        ctrl.load_RF(this->load_RF_s);
        ctrl.sel_TGT(this->sel_TGT_s);
        ctrl.write_mem(this->write_mem_s);
        ctrl.sel_PC(this->sel_PC_s);
        init_values();
    }


    int check() 
    {
       cout << "SIMULAZIONE TERMINATA SENZA ERRORI" << endl;
       return 0;
    }


    private:
    void stimulus_thread()
    {
        cout << "START STIMULUS" << endl << endl;
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            opcode_s.write(opcode_test[i]);
            wait(1,SC_NS);
            cout << "opcode    : " << opcode_s.read() << endl;
            cout << "sel_ALU1  : " << sel_ALU1_s.read() << endl;
            cout << "sel_ALU2  : " << sel_ALU2_s.read() << endl;
            cout << "func_ALU  : " << func_ALU_s.read() << endl;
            cout << "sel_RF    : " << sel_RF_s.read() << endl;
            cout << "load_RF   : " << load_RF_s.read() << endl;
            cout << "sel_TGT   : " << sel_TGT_s.read() << endl;
            cout << "write_mem : " << write_mem_s.read() << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 1;
    unsigned short opcode_test[TEST_SIZE];
    unsigned short eq_test[TEST_SIZE];
    void init_values()
    {

       opcode_test[0]=0;

       eq_test[0]=0;
    }


};

int sc_main(int argc, char* argv[])
{
  int sim_time = 2000;

  TestBench test_ctrl("test_ctrl");

  cout << "START" << endl << endl;

  sc_start(sim_time,SC_NS);

  return test_ctrl.check();
}

